<?php

	namespace LifeOfChaos\EventifyLite\Controller;
	use LifeOfChaos\EventifyLite\DAO\EventDAO;
	use LifeOfChaos\EventifyLite\DAO\RelatedEventsDAO;
	use Psr\Http\Message\ServerRequestInterface as Request;
	use Psr\Http\Message\ResponseInterface as Response;

	class EventController {
		public static function getEvent(Request $request, Response $response, $params) {
			if (empty($params) || !is_array($params) || !array_key_exists('id', $params)) {
				return $response->withJson(
					[
						'status' => false,
						'error' => ['ID de evento obligatorio']
					],
					400
				);
			}
			if (!is_numeric($params['id'])) {
				return $response->withJson(
					[
						'status' => false,
						'error' => ['ID de evento no válido']
					],
					400
				);
			}
			$eventID = (int) $params['id'];
			try {
				$event = EventDAO::getEventByID($eventID);
				if (!$event) {
					return $response->withJson(
						[
							'status' => false,
							'error'  => ['Evento no encontrado']
						],
						404
					);
				}
				$relatedEvents = RelatedEventsDAO::getRelatedEvents($eventID);
				return $response->withJson(
					[
						'status' => true,
						'event'  => $event->toStd(),
						'related_events' => $relatedEvents->getCollection()
					]
				);
			} catch (\Exception $e) {
				return $response->withJson(
					[
						'status' => false,
						'error' => ['Nuestro servicio no está disponible en este momento. Por favor, inténtelo de nuevo más tarde.']
					],
					500
				);
			}
		}
		public static function doSearch(Request $request, Response $response) {
			$postedData = $request->getBody();
			$postedData = json_decode($postedData, true);
			if (is_array($postedData) && (!array_key_exists('startDate', $postedData) || !array_key_exists('endDate', $postedData) || !array_key_exists('maxAssistants', $postedData))) {
				return $response->withJson(
					[
						'status' => false,
						'error' => ['Petición no válida']
					],
					400
				);
			}
			$startDate = \DateTime::createFromFormat('Y-m-d', $postedData['startDate']);
			$endDate = \DateTime::createFromFormat('Y-m-d', $postedData['endDate']);
			$maxAssistants = is_numeric($postedData['maxAssistants']) ? (int) $postedData['maxAssistants'] : false;
			$error = [];
			if (!$startDate) {
				$error[] = 'La fecha de inicio no es válida';
			}
			if (!$endDate) {
				$error[] = 'La fecha de fin no es válida';
			}
			if (!$maxAssistants || $maxAssistants < 0) {
				$error[] = 'La cantidad de máximos asistentes no es válida';
			}
			if ($startDate instanceof \DateTime && $endDate instanceof \DateTime && $startDate > $endDate) {
				$error[] = 'La fecha de fin es anterior a la inicial';
			}
			if (!empty($error)) {
				return $response->withJson(
					[
						'status' => false,
						'error' => $error
					],
					400
				);
			}
			try {
				$events = EventDAO::getEventsBetweenDates($startDate->format('Y-m-d'), $endDate->format('Y-m-d'), $maxAssistants);
				return $response->withJson(
					[
						'status' => true,
						'events' => $events->getCollection()
					]
				);
			} catch (\Exception $e) {
				return $response->withJson(
					[
						'status' => false,
						'error' => ['Nuestro servicio no está disponible en este momento. Por favor, inténtelo de nuevo más tarde.']
					],
					500
				);
			}
		}
	}
