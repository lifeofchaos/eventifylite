<?php

	namespace LifeOfChaos\EventifyLite\Controller;
	use Psr\Http\Message\ServerRequestInterface as Request;
	use Psr\Http\Message\ResponseInterface as Response;

	class Page404Controller {
		public static function render (Request $request, Response $response) {
			return $response->withJson(
				[
					'status' => false,
					'error' => ['Endpoint no implementado']
				],
				501
			);
		}
	}
