<?php

	namespace LifeOfChaos\EventifyLite\Controller;
	use LifeOfChaos\EventifyLite\DAO\EventDAO;
	use LifeOfChaos\EventifyLite\DAO\TicketsDAO;
	use LifeOfChaos\EventifyLite\Model\Ticket;
	use Psr\Http\Message\ServerRequestInterface as Request;
	use Psr\Http\Message\ResponseInterface as Response;

	class TicketsController {
		public static function buy(Request $request, Response $response) {
			$postedData = $request->getBody();
			$postedData = json_decode($postedData, true);
			if (is_array($postedData) && !array_key_exists('event', $postedData)) {
				return $response->withJson(
					[
						'status' => false,
						'error' => ['ID de evento obligatorio']
					],
					400
				);
			}
			if (!is_numeric($postedData['event'])) {
				return $response->withJson(
					[
						'status' => false,
						'error' => ['ID de evento no válido']
					],
					400
				);
			}
			$eventID = (int) $postedData['event'];
			try {
				$event = EventDAO::getEventByID($eventID);
				if (!$event) {
					return $response->withJson(
						[
							'status' => false,
							'error'  => ['No se puede realizar la compra: No se puede encontrar el evento']
						],
						404
					);
				}
			$ticket = new Ticket();
				$ticket->setEventID($eventID)
					->generateBookCode();
			$code = TicketsDAO::save($ticket);
			if (!$code) {
				return $response->withJson(
					[
						'status' => false,
						'error' => ['Nuestro servicio no está disponible en este momento. Por favor, inténtelo de nuevo más tarde.']
					],
					500
				);
			}
			return $response->withJson(
				[
					'status' => true,
					'book_code' => $code
				]
			);
			} catch (\Exception $e) {
				return $response->withJson(
					[
						'status' => false,
						'error' => ['Nuestro servicio no está disponible en este momento. Por favor, inténtelo de nuevo más tarde.']
					],
					500
				);
			}
		}
	}
