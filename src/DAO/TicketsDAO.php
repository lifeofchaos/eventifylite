<?php

	namespace LifeOfChaos\EventifyLite\DAO;
	use LifeOfChaos\EventifyLite\Exception\DatabaseException;
	use LifeOfChaos\EventifyLite\Model\Ticket;

	class TicketsDAO extends BaseDAO {
		public static $tableName = 'events_sold';
		private static function findBookCode($bookCode) : bool {
			$self = self::getInstance();
			$bookCodeQuery = 'SELECT id FROM ' . self::$tableName . ' WHERE book_code = ? LIMIT 1';
			$statement = $self->db->prepare($bookCodeQuery);
			if (!$statement) {
				throw new DatabaseException($self->db->error, $self->db->errno);
			}
			$statement->bind_param('s', $bookCode);
			$statement->execute();
			$statement->bind_result($ticketID);
			$result = $statement->fetch();
			return $result !== null;
		}
		public static function save(Ticket $ticket) {
			$self = self::getInstance();
			try {
				while (self::findBookCode($ticket->getBookCode())) {
					$ticket->generateBookCode();
				}
			} catch (\Exception $e) {
				throw $e;
			}
			$query = 'INSERT INTO ' . self::$tableName . ' (events_id, purchase_date, book_code) VALUES (?, ?, ?)';
			$statement = $self->db->prepare($query);
			if (!$statement) {
				throw new DatabaseException($self->db->error, $self->db->errno);
			}
			[$eventID, $purchaseDate, $bookCode] = [$ticket->getEventID(), date('Y-m-d H:i:s'), $ticket->getBookCode()];
			$statement->bind_param('iss', $eventID, $purchaseDate, $bookCode);
			if ($statement->execute()) {
				return $ticket->getBookCode();
			}
			return false;
		}
	}
