<?php

	namespace LifeOfChaos\EventifyLite\DAO;
	use LifeOfChaos\EventifyLite\Exception\DatabaseException;

	class BaseDAO {
		protected $db;
		private static $_instance;
		public function __construct () {
			$this->db = new \mysqli($_ENV['DB_HOST'], $_ENV['DB_USER'], $_ENV['DB_PASS'], $_ENV['DB_NAME'], !empty($_ENV['DB_PORT']) ? $_ENV['DB_PORT'] : 3306);
			if ($this->db->connect_errno) {
				throw new DatabaseException(
					sprintf(
						'Cannot connect database, MySQL error no %d: %s',
						$this->db->connect_errno,
						$this->db->connect_error),
					$this->db->connect_errno
				);
			}
		}
		public static function getInstance() : self {
			if (empty(self::$_instance)) {
				self::$_instance = new self();
			}
			return self::$_instance;
		}
	}
