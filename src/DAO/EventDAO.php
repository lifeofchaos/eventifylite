<?php

	namespace LifeOfChaos\EventifyLite\DAO;
	use LifeOfChaos\EventifyLite\Exception\DatabaseException;
	use LifeOfChaos\EventifyLite\Model\Event;
	use LifeOfChaos\EventifyLite\Model\EventsCollection;

	class EventDAO extends BaseDAO {
		public static $tableName = 'events';
		public static function getEventByID(int $eventID) : ?Event {
			$self = self::getInstance();
			$query = 'SELECT * FROM ' . self::$tableName . ' WHERE id = ?';
			$statement = $self->db->prepare($query);
			if (!$statement) {
				throw new DatabaseException($self->db->error, $self->db->errno);
			}
			$statement->bind_param('i', $eventID);
			$statement->execute();
			$result = $statement->get_result()->fetch_assoc();
			if ($result) {
				$event = new Event();
				$event->setId((int) $result['id'])
				      ->setName($result['name'])
				      ->setDate($result['date'])
				      ->setPrice($result['price'])
				      ->setMaxAssistants($result['max_assistants'])
				      ->setPopularity($result['popularity'])
				      ->setImage($result['image'])
				      ->setExcerpt($result['excerpt'])
				      ->setDescription($result['description']);
				$result = $event;
			}
			$statement->free_result();
			$statement->close();
			return $result;
		}
		public static function getEventsBetweenDates(string $start, string $end, int $assistants) : EventsCollection {
			$self = self::getInstance();
			$query = 'SELECT * FROM ' . self::$tableName . ' WHERE date BETWEEN ? AND ? AND max_assistants >= ? ORDER BY popularity DESC';
			$statement = $self->db->prepare($query);
			if (!$statement) {
				throw new DatabaseException($self->db->error, $self->db->errno);
			}
			$statement->bind_param('ssi', $start, $end, $assistants);
			$statement->execute();
			$result = $statement->get_result();
			$events = new EventsCollection();
			while ($eventRow = $result->fetch_assoc()) {
				$event = new Event();
				$event->setId((int) $eventRow['id'])
					->setName($eventRow['name'])
					->setDate($eventRow['date'])
					->setPrice($eventRow['price'])
					->setMaxAssistants($eventRow['max_assistants'])
					->setPopularity($eventRow['popularity'])
					->setImage($eventRow['image'])
					->setExcerpt($eventRow['excerpt'])
					->setDescription($eventRow['description']);
				$events->push($event);
			}
			$statement->free_result();
			$statement->close();
			return $events;
		}
	}
