<?php

	namespace LifeOfChaos\EventifyLite\DAO;
	use LifeOfChaos\EventifyLite\Exception\DatabaseException;
	use LifeOfChaos\EventifyLite\Model\Event;
	use LifeOfChaos\EventifyLite\Model\EventsCollection;

	class RelatedEventsDAO extends BaseDAO {
		public static $tableName = 'events_relationship';

		public static function getRelatedEvents(int $eventID) : EventsCollection {
			$self = self::getInstance();
			$query = 'SELECT * FROM ' . self::$tableName . ' as er LEFT JOIN ' . EventDAO::$tableName . ' as e ON er.related_event_id = e.id WHERE events_id = ? ORDER BY e.popularity DESC';
			$statement = $self->db->prepare($query);
			if (!$statement) {
				throw new DatabaseException($self->db->error, $self->db->errno);
			}
			$statement->bind_param('i', $eventID);
			$statement->execute();
			$result = $statement->get_result();
			$events = new EventsCollection();
			while ($eventRow = $result->fetch_assoc()) {
				$event = new Event();
				$event->setId((int) $eventRow['id'])
				      ->setName($eventRow['name'])
				      ->setDate($eventRow['date'])
				      ->setPrice($eventRow['price'])
				      ->setMaxAssistants($eventRow['max_assistants'])
				      ->setPopularity($eventRow['popularity'])
				      ->setImage($eventRow['image'])
				      ->setExcerpt($eventRow['excerpt'])
				      ->setDescription($eventRow['description']);
				if ($event instanceof Event) {
					$events->push($event);
				}
			}
			$statement->free_result();
			$statement->close();
			return $events;
		}
	}
