<?php

	namespace LifeOfChaos\EventifyLite\Model;
	class RelatedEvents {
		/**
		 * @var Event[] $relatedEvents
		 */
		private $relatedEvents;
		/**
		 * @return Event[]
		 */
		public function getRelatedEvents (): array {
			return $this->relatedEvents;
		}
		/**
		 * @param Event[] $relatedEvents
		 *
		 * @return RelatedEvents
		 */
		public function setRelatedEvents (array $relatedEvents): RelatedEvents {
			$this->relatedEvents = $relatedEvents;
			return $this;
		}
	}
