<?php

	namespace LifeOfChaos\EventifyLite\Model;
	class Ticket {
		/**
		 * @var int $id
		 */
		private $id;
		/**
		 * @var int $eventID
		 */
		private $eventID;
		/**
		 * @var string $purchaseDate
		 */
		private $purchaseDate;
		/**
		 * @var string $bookCode
		 */
		private $bookCode;
		/**
		 * @return int
		 */
		public function getId (): int {
			return $this->id;
		}
		/**
		 * @param int $id
		 *
		 * @return Ticket
		 */
		public function setId (int $id): Ticket {
			$this->id = $id;
			return $this;
		}
		/**
		 * @return int
		 */
		public function getEventID (): int {
			return $this->eventID;
		}
		/**
		 * @param int $eventID
		 *
		 * @return Ticket
		 */
		public function setEventID (int $eventID): Ticket {
			$this->eventID = $eventID;
			return $this;
		}
		/**
		 * @return string
		 */
		public function getPurchaseDate (): string {
			return $this->purchaseDate;
		}
		/**
		 * @param string $purchaseDate
		 *
		 * @return Ticket
		 */
		public function setPurchaseDate (string $purchaseDate): Ticket {
			$this->purchaseDate = $purchaseDate;
			return $this;
		}
		/**
		 * @return string
		 */
		public function getBookCode (): string {
			return $this->bookCode;
		}
		/**
		 * @param string $bookCode
		 *
		 * @return Ticket
		 */
		public function setBookCode (string $bookCode): Ticket {
			$this->bookCode = $bookCode;
			return $this;
		}
		public function generateBookCode() : Ticket {
			$this->setBookCode(substr(md5(random_bytes(15)) . sha1(time()), 0, 50));
			return $this;
		}
	}
