<?php

	namespace LifeOfChaos\EventifyLite\Model;
	class Event {
		/**
		 * @var int $id
		 */
		private $id;
		/**
		 * @var string $name
		 */
		private $name;
		/**
		 * @var string $date
		 */
		private $date;
		/**
		 * @var int $price
		 */
		private $price;
		/**
		 * @var int $max_assistants
		 */
		private $max_assistants;
		/**
		 * @var int $popularity
		 */
		private $popularity;
		/**
		 * @var string $image
		 */
		private $image;
		/**
		 * @var string $excerpt
		 */
		private $excerpt;
		/**
		 * @var string $description
		 */
		private $description;
		/**
		 * @return int
		 */
		public function getId (): int {
			return $this->id;
		}
		/**
		 * @param int $id
		 *
		 * @return Event
		 */
		public function setId (int $id): Event {
			$this->id = $id;
			return $this;
		}
		/**
		 * @return string
		 */
		public function getName (): string {
			return $this->name;
		}
		/**
		 * @param string $name
		 *
		 * @return Event
		 */
		public function setName (string $name): Event {
			$this->name = $name;
			return $this;
		}
		/**
		 * @return string
		 */
		public function getDate (): string {
			return $this->date;
		}
		/**
		 * @param string $date
		 *
		 * @return Event
		 */
		public function setDate (string $date): Event {
			$this->date = $date;
			return $this;
		}
		/**
		 * @return int
		 */
		public function getPrice (): int {
			return $this->price;
		}
		/**
		 * @param int $price
		 *
		 * @return Event
		 */
		public function setPrice (int $price): Event {
			$this->price = $price;
			return $this;
		}
		/**
		 * @return int
		 */
		public function getMaxAssistants (): int {
			return $this->max_assistants;
		}
		/**
		 * @param int $max_assistants
		 *
		 * @return Event
		 */
		public function setMaxAssistants (int $max_assistants): Event {
			$this->max_assistants = $max_assistants;
			return $this;
		}
		/**
		 * @return int
		 */
		public function getPopularity (): int {
			return $this->popularity;
		}
		/**
		 * @param int $popularity
		 *
		 * @return Event
		 */
		public function setPopularity (int $popularity): Event {
			$this->popularity = $popularity;
			return $this;
		}
		/**
		 * @return string
		 */
		public function getImage (): string {
			return $this->image;
		}
		/**
		 * @param string $image
		 *
		 * @return Event
		 */
		public function setImage (string $image): Event {
			$this->image = $image;
			return $this;
		}
		/**
		 * @return string
		 */
		public function getExcerpt (): string {
			return $this->excerpt;
		}
		/**
		 * @param string $excerpt
		 *
		 * @return Event
		 */
		public function setExcerpt (?string $excerpt): Event {
			$this->excerpt = $excerpt;
			return $this;
		}
		/**
		 * @return string
		 */
		public function getDescription (): string {
			return $this->description;
		}
		/**
		 * @param string $description
		 *
		 * @return Event
		 */
		public function setDescription (string $description): Event {
			$this->description = $description;
			return $this;
		}
		public function toStd() : \stdClass {
			$props = get_object_vars($this);
			$event = new \stdClass();
			foreach ($props as $prop => $value) {
				$event->$prop = $value;
			}
			return $event;
		}
	}
