<?php

	namespace LifeOfChaos\EventifyLite\Model;
	class EventsCollection {
		/** @var Event[] */
		private $collection = [];
		public function push(Event $event) {
			$this->collection[] = $event;
			return $this;
		}
		function getCollection (): array {
			$events = [];
			foreach ($this->collection as $event) {
				$events[] = $event->toStd();
			}
			return $events;
		}
	}
