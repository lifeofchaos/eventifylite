-- MySQL dump 10.13  Distrib 8.0.16, for macos10.14 (x86_64)
--
-- Host: localhost    Database: p2019_eventify
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `price` int(11) NOT NULL,
  `max_assistants` int(11) NOT NULL,
  `popularity` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `excerpt` text,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,'Evento de DB 1','2019-07-21 00:00:00',100,20,5,'https://source.unsplash.com/1080x450/?event,city,sports,nature?300',NULL,'<h2 style=\"text-align: center;\">&iexcl;Nueva edici&oacute;n de este evento!</h2> <h5 style=\"text-align: center;\">Aqu&iacute; un texto secundario para analizar los formatos de texto.</h5> <p style=\"text-align: center;\"><strong>Ahora un texto en negrita. Finalmente un lorem sin formato.</strong></p> <p>Lorem ipsum dolor sit amet consectetur adipiscing elit nullam, vel non libero sodales quis massa congue eget pretium, interdum enim tincidunt dui suspendisse ante tortor. Cum neque rhoncus libero nisi nibh arcu magna nam, ridiculus mollis placerat sollicitudin senectus fames. Gravida lectus sodales consequat aptent venenatis commodo parturient dignissim phasellus neque, sollicitudin mollis habitant pellentesque ridiculus dis lacus nec in tempor senectus, id ante aliquet pharetra faucibus convallis accumsan eu varius.</p> <p>Vitae cum nostra varius laoreet orci nullam, condimentum pellentesque odio aliquet mi porta lacus, vestibulum tempus molestie maecenas facilisi. Etiam netus cubilia penatibus dictum est nibh ut venenatis conubia erat phasellus, commodo primis parturient lectus ultricies sodales eros mus ornare vivamus fringilla, sociosqu mauris ante suspendisse luctus taciti magnis tortor lacus condimentum. Bibendum dis porta iaculis odio accumsan habitant pharetra, condimentum vestibulum arcu montes cras nulla.</p>'),(2,'Evento de DB 2','2019-07-22 00:00:00',20,2,3,'https://source.unsplash.com/1080x450/?event,city,sports,nature?100','Muy breve descripción del evento...','<h2 style=\"text-align: center;\">&iexcl;Nueva edici&oacute;n de este evento!</h2> <h5 style=\"text-align: center;\">Aqu&iacute; un texto secundario para analizar los formatos de texto.</h5> <p style=\"text-align: center;\"><strong>Ahora un texto en negrita. Finalmente un lorem sin formato.</strong></p> <p>Lorem ipsum dolor sit amet consectetur adipiscing elit nullam, vel non libero sodales quis massa congue eget pretium, interdum enim tincidunt dui suspendisse ante tortor. Cum neque rhoncus libero nisi nibh arcu magna nam, ridiculus mollis placerat sollicitudin senectus fames. Gravida lectus sodales consequat aptent venenatis commodo parturient dignissim phasellus neque, sollicitudin mollis habitant pellentesque ridiculus dis lacus nec in tempor senectus, id ante aliquet pharetra faucibus convallis accumsan eu varius.</p> <p>Vitae cum nostra varius laoreet orci nullam, condimentum pellentesque odio aliquet mi porta lacus, vestibulum tempus molestie maecenas facilisi. Etiam netus cubilia penatibus dictum est nibh ut venenatis conubia erat phasellus, commodo primis parturient lectus ultricies sodales eros mus ornare vivamus fringilla, sociosqu mauris ante suspendisse luctus taciti magnis tortor lacus condimentum. Bibendum dis porta iaculis odio accumsan habitant pharetra, condimentum vestibulum arcu montes cras nulla.</p>'),(3,'Evento de DB 3','2019-07-23 00:00:00',10,1,1,'https://source.unsplash.com/1080x450/?event,city,sports,nature?200',NULL,'<h2 style=\"text-align: center;\">&iexcl;Nueva edici&oacute;n de este evento!</h2> <h5 style=\"text-align: center;\">Aqu&iacute; un texto secundario para analizar los formatos de texto.</h5> <p style=\"text-align: center;\"><strong>Ahora un texto en negrita. Finalmente un lorem sin formato.</strong></p> <p>Lorem ipsum dolor sit amet consectetur adipiscing elit nullam, vel non libero sodales quis massa congue eget pretium, interdum enim tincidunt dui suspendisse ante tortor. Cum neque rhoncus libero nisi nibh arcu magna nam, ridiculus mollis placerat sollicitudin senectus fames. Gravida lectus sodales consequat aptent venenatis commodo parturient dignissim phasellus neque, sollicitudin mollis habitant pellentesque ridiculus dis lacus nec in tempor senectus, id ante aliquet pharetra faucibus convallis accumsan eu varius.</p> <p>Vitae cum nostra varius laoreet orci nullam, condimentum pellentesque odio aliquet mi porta lacus, vestibulum tempus molestie maecenas facilisi. Etiam netus cubilia penatibus dictum est nibh ut venenatis conubia erat phasellus, commodo primis parturient lectus ultricies sodales eros mus ornare vivamus fringilla, sociosqu mauris ante suspendisse luctus taciti magnis tortor lacus condimentum. Bibendum dis porta iaculis odio accumsan habitant pharetra, condimentum vestibulum arcu montes cras nulla.</p>'),(4,'Evento de DB 4','2019-07-23 00:00:00',30,5,3,'https://source.unsplash.com/1080x450/?event,city,sports,nature?400',NULL,'<h2 style=\"text-align: center;\">&iexcl;Nueva edici&oacute;n de este evento!</h2> <h5 style=\"text-align: center;\">Aqu&iacute; un texto secundario para analizar los formatos de texto.</h5> <p style=\"text-align: center;\"><strong>Ahora un texto en negrita. Finalmente un lorem sin formato.</strong></p> <p>Lorem ipsum dolor sit amet consectetur adipiscing elit nullam, vel non libero sodales quis massa congue eget pretium, interdum enim tincidunt dui suspendisse ante tortor. Cum neque rhoncus libero nisi nibh arcu magna nam, ridiculus mollis placerat sollicitudin senectus fames. Gravida lectus sodales consequat aptent venenatis commodo parturient dignissim phasellus neque, sollicitudin mollis habitant pellentesque ridiculus dis lacus nec in tempor senectus, id ante aliquet pharetra faucibus convallis accumsan eu varius.</p> <p>Vitae cum nostra varius laoreet orci nullam, condimentum pellentesque odio aliquet mi porta lacus, vestibulum tempus molestie maecenas facilisi. Etiam netus cubilia penatibus dictum est nibh ut venenatis conubia erat phasellus, commodo primis parturient lectus ultricies sodales eros mus ornare vivamus fringilla, sociosqu mauris ante suspendisse luctus taciti magnis tortor lacus condimentum. Bibendum dis porta iaculis odio accumsan habitant pharetra, condimentum vestibulum arcu montes cras nulla.</p>'),(5,'Evento de DB 5','2019-07-23 00:00:00',24,10,5,'https://source.unsplash.com/1080x450/?event,city,sports,nature?500',NULL,'<h2 style=\"text-align: center;\">&iexcl;Nueva edici&oacute;n de este evento!</h2> <h5 style=\"text-align: center;\">Aqu&iacute; un texto secundario para analizar los formatos de texto.</h5> <p style=\"text-align: center;\"><strong>Ahora un texto en negrita. Finalmente un lorem sin formato.</strong></p> <p>Lorem ipsum dolor sit amet consectetur adipiscing elit nullam, vel non libero sodales quis massa congue eget pretium, interdum enim tincidunt dui suspendisse ante tortor. Cum neque rhoncus libero nisi nibh arcu magna nam, ridiculus mollis placerat sollicitudin senectus fames. Gravida lectus sodales consequat aptent venenatis commodo parturient dignissim phasellus neque, sollicitudin mollis habitant pellentesque ridiculus dis lacus nec in tempor senectus, id ante aliquet pharetra faucibus convallis accumsan eu varius.</p> <p>Vitae cum nostra varius laoreet orci nullam, condimentum pellentesque odio aliquet mi porta lacus, vestibulum tempus molestie maecenas facilisi. Etiam netus cubilia penatibus dictum est nibh ut venenatis conubia erat phasellus, commodo primis parturient lectus ultricies sodales eros mus ornare vivamus fringilla, sociosqu mauris ante suspendisse luctus taciti magnis tortor lacus condimentum. Bibendum dis porta iaculis odio accumsan habitant pharetra, condimentum vestibulum arcu montes cras nulla.</p>'),(6,'Evento de DB 6','2019-07-24 00:00:00',34,13,1,'https://source.unsplash.com/1080x450/?event,city,sports,nature?600',NULL,'<h2 style=\"text-align: center;\">&iexcl;Nueva edici&oacute;n de este evento!</h2> <h5 style=\"text-align: center;\">Aqu&iacute; un texto secundario para analizar los formatos de texto.</h5> <p style=\"text-align: center;\"><strong>Ahora un texto en negrita. Finalmente un lorem sin formato.</strong></p> <p>Lorem ipsum dolor sit amet consectetur adipiscing elit nullam, vel non libero sodales quis massa congue eget pretium, interdum enim tincidunt dui suspendisse ante tortor. Cum neque rhoncus libero nisi nibh arcu magna nam, ridiculus mollis placerat sollicitudin senectus fames. Gravida lectus sodales consequat aptent venenatis commodo parturient dignissim phasellus neque, sollicitudin mollis habitant pellentesque ridiculus dis lacus nec in tempor senectus, id ante aliquet pharetra faucibus convallis accumsan eu varius.</p> <p>Vitae cum nostra varius laoreet orci nullam, condimentum pellentesque odio aliquet mi porta lacus, vestibulum tempus molestie maecenas facilisi. Etiam netus cubilia penatibus dictum est nibh ut venenatis conubia erat phasellus, commodo primis parturient lectus ultricies sodales eros mus ornare vivamus fringilla, sociosqu mauris ante suspendisse luctus taciti magnis tortor lacus condimentum. Bibendum dis porta iaculis odio accumsan habitant pharetra, condimentum vestibulum arcu montes cras nulla.</p>'),(7,'Evento de DB 7','2019-07-24 00:00:00',43,11,2,'https://source.unsplash.com/1080x450/?event,city,sports,nature?700','Muy breve descripción del evento, otra más...','<h2 style=\"text-align: center;\">&iexcl;Nueva edici&oacute;n de este evento!</h2> <h5 style=\"text-align: center;\">Aqu&iacute; un texto secundario para analizar los formatos de texto.</h5> <p style=\"text-align: center;\"><strong>Ahora un texto en negrita. Finalmente un lorem sin formato.</strong></p> <p>Lorem ipsum dolor sit amet consectetur adipiscing elit nullam, vel non libero sodales quis massa congue eget pretium, interdum enim tincidunt dui suspendisse ante tortor. Cum neque rhoncus libero nisi nibh arcu magna nam, ridiculus mollis placerat sollicitudin senectus fames. Gravida lectus sodales consequat aptent venenatis commodo parturient dignissim phasellus neque, sollicitudin mollis habitant pellentesque ridiculus dis lacus nec in tempor senectus, id ante aliquet pharetra faucibus convallis accumsan eu varius.</p> <p>Vitae cum nostra varius laoreet orci nullam, condimentum pellentesque odio aliquet mi porta lacus, vestibulum tempus molestie maecenas facilisi. Etiam netus cubilia penatibus dictum est nibh ut venenatis conubia erat phasellus, commodo primis parturient lectus ultricies sodales eros mus ornare vivamus fringilla, sociosqu mauris ante suspendisse luctus taciti magnis tortor lacus condimentum. Bibendum dis porta iaculis odio accumsan habitant pharetra, condimentum vestibulum arcu montes cras nulla.</p>'),(8,'Evento de DB 8','2019-07-25 00:00:00',5,17,3,'https://source.unsplash.com/1080x450/?event,city,sports,nature?800',NULL,'<h2 style=\"text-align: center;\">&iexcl;Nueva edici&oacute;n de este evento!</h2> <h5 style=\"text-align: center;\">Aqu&iacute; un texto secundario para analizar los formatos de texto.</h5> <p style=\"text-align: center;\"><strong>Ahora un texto en negrita. Finalmente un lorem sin formato.</strong></p> <p>Lorem ipsum dolor sit amet consectetur adipiscing elit nullam, vel non libero sodales quis massa congue eget pretium, interdum enim tincidunt dui suspendisse ante tortor. Cum neque rhoncus libero nisi nibh arcu magna nam, ridiculus mollis placerat sollicitudin senectus fames. Gravida lectus sodales consequat aptent venenatis commodo parturient dignissim phasellus neque, sollicitudin mollis habitant pellentesque ridiculus dis lacus nec in tempor senectus, id ante aliquet pharetra faucibus convallis accumsan eu varius.</p> <p>Vitae cum nostra varius laoreet orci nullam, condimentum pellentesque odio aliquet mi porta lacus, vestibulum tempus molestie maecenas facilisi. Etiam netus cubilia penatibus dictum est nibh ut venenatis conubia erat phasellus, commodo primis parturient lectus ultricies sodales eros mus ornare vivamus fringilla, sociosqu mauris ante suspendisse luctus taciti magnis tortor lacus condimentum. Bibendum dis porta iaculis odio accumsan habitant pharetra, condimentum vestibulum arcu montes cras nulla.</p>'),(9,'Evento de DB 9','2020-07-22 00:00:00',67,19,3,'https://source.unsplash.com/1080x450/?event,city,sports,nature?900',NULL,'<h2 style=\"text-align: center;\">&iexcl;Nueva edici&oacute;n de este evento!</h2> <h5 style=\"text-align: center;\">Aqu&iacute; un texto secundario para analizar los formatos de texto.</h5> <p style=\"text-align: center;\"><strong>Ahora un texto en negrita. Finalmente un lorem sin formato.</strong></p> <p>Lorem ipsum dolor sit amet consectetur adipiscing elit nullam, vel non libero sodales quis massa congue eget pretium, interdum enim tincidunt dui suspendisse ante tortor. Cum neque rhoncus libero nisi nibh arcu magna nam, ridiculus mollis placerat sollicitudin senectus fames. Gravida lectus sodales consequat aptent venenatis commodo parturient dignissim phasellus neque, sollicitudin mollis habitant pellentesque ridiculus dis lacus nec in tempor senectus, id ante aliquet pharetra faucibus convallis accumsan eu varius.</p> <p>Vitae cum nostra varius laoreet orci nullam, condimentum pellentesque odio aliquet mi porta lacus, vestibulum tempus molestie maecenas facilisi. Etiam netus cubilia penatibus dictum est nibh ut venenatis conubia erat phasellus, commodo primis parturient lectus ultricies sodales eros mus ornare vivamus fringilla, sociosqu mauris ante suspendisse luctus taciti magnis tortor lacus condimentum. Bibendum dis porta iaculis odio accumsan habitant pharetra, condimentum vestibulum arcu montes cras nulla.</p>'),(10,'Evento de DB 10','2099-07-22 00:00:00',3,13,4,'https://source.unsplash.com/1080x450/?event,city,sports,nature?1000',NULL,'<h2 style=\"text-align: center;\">&iexcl;Nueva edici&oacute;n de este evento!</h2> <h5 style=\"text-align: center;\">Aqu&iacute; un texto secundario para analizar los formatos de texto.</h5> <p style=\"text-align: center;\"><strong>Ahora un texto en negrita. Finalmente un lorem sin formato.</strong></p> <p>Lorem ipsum dolor sit amet consectetur adipiscing elit nullam, vel non libero sodales quis massa congue eget pretium, interdum enim tincidunt dui suspendisse ante tortor. Cum neque rhoncus libero nisi nibh arcu magna nam, ridiculus mollis placerat sollicitudin senectus fames. Gravida lectus sodales consequat aptent venenatis commodo parturient dignissim phasellus neque, sollicitudin mollis habitant pellentesque ridiculus dis lacus nec in tempor senectus, id ante aliquet pharetra faucibus convallis accumsan eu varius.</p> <p>Vitae cum nostra varius laoreet orci nullam, condimentum pellentesque odio aliquet mi porta lacus, vestibulum tempus molestie maecenas facilisi. Etiam netus cubilia penatibus dictum est nibh ut venenatis conubia erat phasellus, commodo primis parturient lectus ultricies sodales eros mus ornare vivamus fringilla, sociosqu mauris ante suspendisse luctus taciti magnis tortor lacus condimentum. Bibendum dis porta iaculis odio accumsan habitant pharetra, condimentum vestibulum arcu montes cras nulla.</p>'),(11,'Evento de DB 11','2019-07-30 00:00:00',5,6,5,'https://source.unsplash.com/1080x450/?event,city,sports,nature?1100',NULL,'<h2 style=\"text-align: center;\">&iexcl;Nueva edici&oacute;n de este evento!</h2> <h5 style=\"text-align: center;\">Aqu&iacute; un texto secundario para analizar los formatos de texto.</h5> <p style=\"text-align: center;\"><strong>Ahora un texto en negrita. Finalmente un lorem sin formato.</strong></p> <p>Lorem ipsum dolor sit amet consectetur adipiscing elit nullam, vel non libero sodales quis massa congue eget pretium, interdum enim tincidunt dui suspendisse ante tortor. Cum neque rhoncus libero nisi nibh arcu magna nam, ridiculus mollis placerat sollicitudin senectus fames. Gravida lectus sodales consequat aptent venenatis commodo parturient dignissim phasellus neque, sollicitudin mollis habitant pellentesque ridiculus dis lacus nec in tempor senectus, id ante aliquet pharetra faucibus convallis accumsan eu varius.</p> <p>Vitae cum nostra varius laoreet orci nullam, condimentum pellentesque odio aliquet mi porta lacus, vestibulum tempus molestie maecenas facilisi. Etiam netus cubilia penatibus dictum est nibh ut venenatis conubia erat phasellus, commodo primis parturient lectus ultricies sodales eros mus ornare vivamus fringilla, sociosqu mauris ante suspendisse luctus taciti magnis tortor lacus condimentum. Bibendum dis porta iaculis odio accumsan habitant pharetra, condimentum vestibulum arcu montes cras nulla.</p>');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events_relationship`
--

DROP TABLE IF EXISTS `events_relationship`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `events_relationship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `events_id` int(11) NOT NULL,
  `related_event_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events_relationship`
--

LOCK TABLES `events_relationship` WRITE;
/*!40000 ALTER TABLE `events_relationship` DISABLE KEYS */;
INSERT INTO `events_relationship` VALUES (22,1,2),(23,1,4),(24,1,6),(25,3,5),(26,5,3),(27,11,1),(28,11,2),(29,11,3),(30,11,4),(31,2,4),(32,3,5),(33,3,6),(34,6,5),(35,6,7),(36,5,2),(37,4,5),(38,4,8),(39,4,1),(40,8,7),(41,9,1),(42,9,2),(43,9,3),(44,10,2);
/*!40000 ALTER TABLE `events_relationship` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events_sold`
--

DROP TABLE IF EXISTS `events_sold`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `events_sold` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `events_id` int(11) NOT NULL,
  `purchase_date` datetime NOT NULL,
  `book_code` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_events_sold_events1_idx` (`events_id`),
  CONSTRAINT `fk_events_sold_events1` FOREIGN KEY (`events_id`) REFERENCES `events` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events_sold`
--

LOCK TABLES `events_sold` WRITE;
/*!40000 ALTER TABLE `events_sold` DISABLE KEYS */;
INSERT INTO `events_sold` VALUES (14,7,'2019-07-21 17:02:04','f03ad922a37b1c07ce94c8109cdf860aca63919aeeca4b0daa');
/*!40000 ALTER TABLE `events_sold` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-21 19:03:08
