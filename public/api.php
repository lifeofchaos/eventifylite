<?php
	require __DIR__ . '/../src/vendor/autoload.php';
	use Symfony\Component\Dotenv\Dotenv;
	use Slim\App;
	use LifeOfChaos\EventifyLite\Controller\EventController;
	use Slim\Container;
	use LifeOfChaos\EventifyLite\Controller\Page404Controller;
	use LifeOfChaos\EventifyLite\Controller\TicketsController;


	// Load ENV vars
	$dotenv = new Dotenv();
	$dotenv->load(__DIR__.'/../config/.env');

	$container = new Container();

	$container['notFoundHandler'] = array(Page404Controller::class, 'render');

	$app = new App($container);

	$app->group('/api/v0', function () use ($app) {
		$app->post('/search[/]', array (EventController::class, 'doSearch'));
		$app->get('/events/{id}[/]', array (EventController::class, 'getEvent'));
		$app->post('/buy[/]', array (TicketsController::class, 'buy'));
	});

	$app->run();

