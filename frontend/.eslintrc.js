module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    // Make JavaScript serious again, here are some of my favourite rules
    'generator-star-spacing': 'off',
    "space-before-function-paren": [
        "error",
        {
          "named": "never"
        }
      ],
    "semi": [
      1,
      "always"
    ]
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
