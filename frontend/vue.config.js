const webpack = require('webpack');
const path = require('path');

module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '../dist' : '/',
  outputDir: process.env.NODE_ENV === 'production' ? path.resolve(__dirname, '../public/dist/') : 'dist',
  chainWebpack: config => {
    if (process.env.NODE_ENV === 'production') {
      config
        .plugin('html')
        .tap(args => {
          args[0].filename = path.resolve(__dirname, '../public/index.html');
          args[0].template = path.resolve(__dirname, './public/index.html');
          return args;
        });
    }
  },
  configureWebpack: {
    plugins: [
      new webpack.IgnorePlugin(
        {
          resourceRegExp: /^\.\/locale$/,
          contextRegExp: /moment$/
        }
      )
    ]
  },
  devServer: {
    host: 'eventify.test',
    open: 'Google Chrome',
    overlay: true,
    watchOptions: {
      disableHostCheck: true
    },
    proxy: {
      '/api/v0/': {
        target: 'http://eventify.test/api/v0/',
        pathRewrite: {'^/api/v0/' : ''}
      }
    }
  }
};
