import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loader: {
      display: false,
      text: 'Cargando...'
    },
    errorDialog: {
      display: false,
      text: null
    }
  },
  getters: {
    loader: state => {
      return state.loader;
    },
    errorDialog: state => {
      return state.errorDialog;
    }
  },
  mutations: {
    SET_LOADER(state, loader) {
      state.loader = loader;
    },
    SET_ERROR_DIALOG(state, message) {
      state.errorDialog = {
        display: true,
        text: message
      };
    },
    RESET_ERROR_DIALOG(state) {
      state.errorDialog = {
        display: false
      };
    },
    RESET_LOADER(state) {
      state.loader = {
        display: false
      };
    }
  },
  actions: {
    displayErrorDialog(context, text) {
      context.commit('SET_ERROR_DIALOG', text);
    },
    hideErrorDialog(context) {
      context.commit('RESET_ERROR_DIALOG');
    },
    displayLoader(context, text) {
      text = text || 'Cargando...';
      context.commit('SET_LOADER', { display: true, text });
    },
    hideLoader(context) {
      context.commit('RESET_LOADER');
    }
  }
});
