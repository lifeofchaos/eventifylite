import Api from '../services/api';
import Vue from 'vue';

Vue.use({
  install(Vue, opts) {
    Vue.prototype.$api = new Proxy(new Api(), {
      get(target, name) {
        switch (name) {
          case 'search':
            return (start, end, assistants) => {
              return target.findEvents(start, end, assistants);
            };
          case 'event':
            return (eventID) => {
              return target.getEvent(eventID);
            };
          case 'buy':
            return (eventID) => {
              return target.buyEvent(eventID);
            };
          default:
            throw new Error('Unknown method');
        }
      }
    });
  }
});
