import Error404 from '../error/Error404';
import Error500 from '../error/Error500';
import Error400 from '../error/Error400';

class ErrorInterceptorService {
  static catchError(error) {
    if (error.response) {
      let handler = {
        get: (target, name) => {
          if (target.hasOwnProperty(name)) {
            target[name](error);
          } else {
            throw new Error('Unhandled Error');
          }
        }
      };
      let proxy = new Proxy(ErrorInterceptorService, handler);
      return proxy['error' + error.response.status + 'Interceptor'];
    } else if (error.request) {
      throw new Error500('No se puede conectar a la API');
    } else {
      throw error;
    }
  }
  static error400Interceptor(error) {
    if (error.hasOwnProperty('response') && error.response.hasOwnProperty('data') && error.response.data.hasOwnProperty('error')) {
      throw new Error400(error.response.data.error.join(', '));
    }
    throw error;
  }
  static error500Interceptor() {
    throw new Error500('Servicio no disponible');
  }
  static error404Interceptor(error) {
    if (error.hasOwnProperty('response') && error.response.hasOwnProperty('data') && error.response.data.hasOwnProperty('error')) {
      throw new Error404(error.response.data.error.join(', '));
    }
    throw error;
  }
}
export default ErrorInterceptorService;
