import axios from 'axios';
import ErrorInterceptorService from './ErrorInterceptorService';

class Api {
  /**
   * Constructor
   * Sets API base URL for Axios request
   * @constructor
   */
  constructor() {
    this.axios = axios.create({
      baseURL: '/api/v0/',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    });
    this.axios.interceptors.response.use(response => response, ErrorInterceptorService.catchError);
  }
  findEvents(startDate, endDate, maxAssistants) {
    return this.axios.post(
      'search/',
      {
        startDate,
        endDate,
        maxAssistants
      }
    );
  }
  getEvent(eventID) {
    return this.axios.get(
      'events/' + eventID
    );
  }
  buyEvent(eventID) {
    return this.axios.post(
      'buy',
      {
        event: eventID
      }
    );
  }
}

export default Api;
