import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/event/:event',
      props: (route) => {
        return {
          event: Number.parseInt(route.params.event)
        };
      },
      name: 'event',
      component: () => import(/* webpackChunkName: "event" */ './views/Event.vue')
    },
    {
      path: '/500',
      props: true,
      name: '500',
      component: () => import(/* webpackChunkName: "page500" */ './views/Page500.vue')
    },
    {
      path: '/404',
      props: true,
      name: '404',
      component: () => import(/* webpackChunkName: "page404" */ './views/Page404.vue')
    },
    {
      path: '/confirmation',
      props: true,
      name: 'ViewTicket',
      component: () => import(/* webpackChunkName: "viewTicket" */ './views/ViewTicket.vue')
    },
    {
      path: '*',
      redirect: {
        name: '404'
      }
    }
  ]
});
