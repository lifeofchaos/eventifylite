class Error500 extends Error {
  constructor(message) {
    super(message);
    this.name = 'Error500';
  }
}

export default Error500;
